#include <iostream>
#include "fahrenheit.hpp"

Fahrenheit::Fahrenheit(float temperatura) {
	setTemperatura(temperatura);
}

float Fahrenheit::converteTemperatura1(){
	
		setTempConv1(((getTemperatura() - 32) * 5) / 9);
		return getTempConv1();
}

float Fahrenheit::converteTemperatura2(){
	
		setTempConv2((((getTemperatura() - 32) * 5) / 9) + 273);
		return getTempConv2();
}

