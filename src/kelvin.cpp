#include <iostream>
#include "kelvin.hpp"

Kelvin::Kelvin(float temperatura) {
	setTemperatura(temperatura);
}

float Kelvin::converteTemperatura1(){
		
		setTempConv1(getTemperatura() - 273);		
		return getTempConv1();
}

float Kelvin::converteTemperatura2(){
		
		setTempConv2((((getTemperatura() - 273) / 5 ) * 9 ) + 32);		
		return getTempConv2();
}
