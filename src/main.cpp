#include <iostream>
#include "fahrenheit.hpp"
#include "celsius.hpp"
#include "kelvin.hpp"

using namespace std;

int main() {
	
	Celsius * temp1 = new Celsius(20.0);
	Kelvin * temp2 = new Kelvin(273.0);
	Fahrenheit * temp3 = new Fahrenheit(140.0);
	
	temp1->converteTemperatura1();
	temp1->converteTemperatura2();
	temp2->converteTemperatura1();
	temp2->converteTemperatura2();
	temp3->converteTemperatura1();
	temp3->converteTemperatura2();
	
	cout << "A temperatura " << temp1->getTemperatura() << " celsius convertida para kelvin: " << temp1->getTempConv2() << " e convertida para fahrenheit: " << temp1->getTempConv1() << endl;
	cout << "A temperatura " << temp2->getTemperatura() << " kelvin convertida para celsius: " << temp2->getTempConv1() << " e convertida para fahrenheit: " << temp2->getTempConv2() << endl;
	cout << "A temperatura " << temp3->getTemperatura() << " fahrenheit convertida para celsius: " << temp3->getTempConv1() << " e convertida para kelvin: " << temp3->getTempConv2() << endl;
	
	delete(temp1);
	delete(temp2);
	delete(temp3);

	return 0;
}
