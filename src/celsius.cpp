#include <iostream>
#include "celsius.hpp"

Celsius::Celsius(float temperatura) {
	setTemperatura(temperatura);
}

float Celsius::converteTemperatura1(){
		
		setTempConv1(((getTemperatura()/5) * 9) + 32);		
		return getTempConv1();
}

float Celsius::converteTemperatura2(){
		
		setTempConv2(getTemperatura() + 273);		
		return getTempConv2();
}
