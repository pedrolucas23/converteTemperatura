#include <iostream>
#include "medidorTemperatura.hpp"

using namespace std;

MedidorTemperatura::MedidorTemperatura() {
	setTemperatura(0);
}

void MedidorTemperatura::setTemperatura(float temperatura) {
	this->temperatura = temperatura;
}

void MedidorTemperatura::setTempConv1(float tempConv1) {
	this->tempConv1 = tempConv1;
}

void MedidorTemperatura::setTempConv2(float tempConv2) {
	this->tempConv2 = tempConv2;
}

float MedidorTemperatura::getTemperatura() {
	return temperatura;
}

float MedidorTemperatura::getTempConv1() {
	return tempConv1;
}

float MedidorTemperatura::getTempConv2() {
	return tempConv2;
}

float MedidorTemperatura::converteTemperatura1() {
	this->tempConv1 = tempConv1;
	return this->tempConv1;
}

float MedidorTemperatura::converteTemperatura2() {
	this->tempConv2 = tempConv2;
	return this->tempConv2;
}
