#ifndef MEDIDORTEMPERATURA_H
#define MEDIDORTEMPERATURA_H

class MedidorTemperatura{

	private:
		float temperatura, tempConv1, tempConv2;
		
	public:
		MedidorTemperatura();
		//~MedidorTemperatura();		
		
		float getTemperatura();
		float getTempConv1();
		float getTempConv2();
		void setTemperatura(float temperatura);
		
		float converteTemperatura1();
		float converteTemperatura2();
		
	protected:
		void setTempConv1(float tempConv1);
		void setTempConv2(float tempConv2);
		
		
};

#endif
