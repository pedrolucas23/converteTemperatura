#ifndef KELVIN_H
#define KELVIN_H

#include "medidorTemperatura.hpp"

 class Kelvin : public MedidorTemperatura{
 
 	public:
 		Kelvin();
 		Kelvin(float temperatura);
 		
 		float converteTemperatura1();
 		float converteTemperatura2();
 		
 };
 
 #endif
 		
