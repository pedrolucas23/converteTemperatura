#ifndef CELSIUS_H
#define CELSIUS_H

#include "medidorTemperatura.hpp"

 class Celsius : public MedidorTemperatura{
 
 	public:
 		Celsius();
 		Celsius(float temperatura); 
 		
 		float converteTemperatura1();
 		float converteTemperatura2();		
 };
 
 #endif
 		
