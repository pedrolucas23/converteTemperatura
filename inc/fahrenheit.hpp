#ifndef FAHRENHEIT_H
#define FAHRENHEIT_H

#include "medidorTemperatura.hpp"

 class Fahrenheit : public MedidorTemperatura{
 
 	public:
 		Fahrenheit();
 		Fahrenheit(float temperatura);
 		
 		float converteTemperatura1();
 		float converteTemperatura2();
 		
 };
 
 #endif
 		
